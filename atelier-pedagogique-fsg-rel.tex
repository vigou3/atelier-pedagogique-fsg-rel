%%% Copyright (C) 2023 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Atelier d'échange pédagogique de
%%% la Faculté des sciences et de génie - Ressources éducatives
%%% libres» <https://gitlab.com/vigou3/atelier-pedagogique-fsg-rel>
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0 International de
%%% Creative Commons. <https://creativecommons.org/licenses/by-sa/4.0>

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage[round]{natbib}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{fontawesome5}              % icônes
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% =============================
  %%  Informations de publication
  %% =============================
  \title{Ressources éducatives libres}
  \author{Vincent Goulet}
  \renewcommand{\year}{2023}
  \renewcommand{\month}{12}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/atelier-pedagogique-fsg-rel}

  %% =======================
  %%  Apparence du document
  %% =======================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]

  %% Couleurs additionnelles
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}            % bandeau or UL
  \colorlet{alert}{mLightBrown} % alias pour couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias pour couleur Metropolis
  \colorlet{code}{mLightGreen}  % alias pour couleur Metropolis

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Ressources éducatives libres},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Renvoi vers GitLab sur la page de copyright
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}     \newlength{\imagewidth}
  \newlength{\logoheight}

\begin{document}

%% frontmatter
\input{couverture-avant}
\input{notices}

%% mainmatter
\begin{frame}[plain]
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,0\TPVertModule)
    \textcolor{dark}{\rule{\linewidth}{64\TPVertModule}}
  \end{textblock*}

  \begin{minipage}{0.5\linewidth}
    \textbf{Origines du mouvement libre: logiciel}
    \begin{itemize}
    \item Richard Stallman et projet GNU (1983)
    \item \emph{GNU General Public License} (GPL, 1989)
    \end{itemize}
    \onslide<2->
    {
      \textbf{Libertés essentielles}
      \begin{enumerate}
        \setcounter{enumi}{-1}
      \item Exécuter
      \item Étudier
      \item Redistribuer
      \item Modifier
      \end{enumerate}
    }
  \end{minipage}
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,3\TPVertModule)
    \centering
    \includegraphics[width=0.7\linewidth,keepaspectratio]{images/Heckert_GNU_white.svg.png}
  \end{textblock*}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,0\TPVertModule)
    \textcolor{dark}{\rule{\linewidth}{64\TPVertModule}}
  \end{textblock*}

  \begin{minipage}{0.5\linewidth}
    \raggedright
    \textbf{Mes premières contributions}

    \begin{itemize}
    \item \emph{Emacs Modified} ($\sim$2001) \\
      Logiciel --- Licence GNU GPL
    \item Paquetage R \textbf{actuar} (2005) \\
      Logiciel --- Licence GNU GPL
    \item «Introduction à la programmation \\ avec S» (2006) \\
      Manuel --- Licence GNU FDL
    \end{itemize}
  \end{minipage}
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,4\TPVertModule)
    \centering
    \includegraphics[width=0.3\linewidth,keepaspectratio]{images/EmacsIcon.svg.png} \\
    \bigskip
    \includegraphics[width=0.3\linewidth,keepaspectratio]{images/Rlogo}
  \end{textblock*}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,0\TPVertModule)
    \textcolor{dark}{\rule{\linewidth}{64\TPVertModule}}
  \end{textblock*}

  \begin{minipage}{0.5\linewidth}
    \raggedright
    \textbf{Décennie 2000: diversification et percée dans l'enseignement}
    \begin{itemize}
    \item OpenCourseWare
    \item Creative Commons
    \item Plan stratégique FSG 2009-2015
    \end{itemize}
  \end{minipage}
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,5.3\TPVertModule)
    \centering
    {
      \color{white}
      \fontsize{42}{42}
      \faCreativeCommons\par
      \fontsize{28}{28}
      \faCreativeCommonsBy\quad
      \faCreativeCommonsSa\quad
      \faCreativeCommonsNc\quad
      \faCreativeCommonsNd\par
    }
  \end{textblock*}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,0\TPVertModule)
    \textcolor{dark}{\rule{\linewidth}{64\TPVertModule}}
  \end{textblock*}

  \begin{minipage}{0.5\linewidth}
    \raggedright
    \textbf{Portail libre de l'École d'actuariat}
    \begin{itemize}
    \item Lancement en 2010
    \item REL pour 9 cours différents
    \item Recueils d'exercices en format PDF
    \item \nombre{1134} \emph{solutions-clips}
    \end{itemize}
  \end{minipage}
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,5.2\TPVertModule)
    \centering
    \includegraphics[width=0.8\linewidth,keepaspectratio]{images/portail-libre-act}
  \end{textblock*}
\end{frame}

\begin{frame}[plain]
  \textbf{Ma production de REL au fil des années...}

  \setlength{\fboxsep}{0pt}
  \begin{tabular*}{0.5\linewidth}{@{}ccc}
    %% 0.15\linewidth = 0.3 x 0.5\linewidth
      \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/01-programmer-avec-r}}
    & \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/02-methodes-numeriques-en-actuariat-avec-r}}
    & \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/03-theorie-credibilite-avec-r}} \\
      \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/04-formation-latex-ul}}
    & \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/05-modelisation-distributions-sinistres-avec-r}}
    & \fbox{\includegraphics[width=0.15\linewidth,keepaspectratio]{images/06-analyse-statistique-exercices-et-solutions}}
  \end{tabular*}
  \hfill
  \begin{tabular*}{0.45\linewidth}{cc}
    %% 0.2\linewidth ~= 0.45 x 0.45\linewidth
      \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/01-ligne-commande-unix}}
    & \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/02-presentation-r}} \\
      \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/03-gestion-versions-avec-git}}
    & \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/04-laboratoire-rmarkdown}} \\
      \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/05-laboratoire-paquetages-r}}
    & \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/06-laboratoire-shiny}} \\
      \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/08-ift-4902-classes-maitre}}
    & \fbox{\includegraphics[width=0.2\linewidth,keepaspectratio]{images/51-atelier-science-donnees}}
  \end{tabular*}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,0\TPVertModule)
    \textcolor{dark}{\rule{\linewidth}{64\TPVertModule}}
  \end{textblock*}

  \begin{minipage}{0.5\linewidth}
    \textbf{Pourquoi publier des REL? (Mes raisons)}

    \begin{itemize}
    \item Ne pas usurper de droits d'auteur
    \item Simplifier la gestion des droits d'auteur
    \item Redonner à la communauté
    \item Diffuser le savoir
    \item Faciliter la publication
    \end{itemize}
  \end{minipage}

  \begin{textblock*}{7.2\TPHorizModule}(8.8\TPHorizModule,5.5\TPVertModule)
    \centering
    {
      \color{white}
      \fontsize{28}{28}
      \faCopyright[regular]\quad
      \faCreativeCommonsBy\quad
      \faCreativeCommonsSa \\[12pt]
      \faBroadcastTower\quad
      \faCloudUpload*\par
    }
  \end{textblock*}
\end{frame}

\begin{frame}[standout]
  \begin{itemize}
    %% hack horrible pour tout aligner à gauche
  \item \textbf{PRINCIPAUX ENJEUX} \\[12pt]
  \item Diffuser les ressources
  \item Utiliser un format universel et pérenne
  \item Financer la production et la diffusion
  \end{itemize}
\end{frame}

%% backmatter
\input{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
