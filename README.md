# Ressources éducatives libres (REL)

Matériel d'une présentation effectuée en compagnie de Nadia Aubin-Horth (Département de biologie) et Anne-Marie Duchesneau (Bibliothèque) dans le cadre des activités d'échange pédagogique de la [Faculté des sciences et de génie](https://fsg.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

## Auteur

Vincent Goulet, École d'actuariat, Université Laval  

## Résumé

Cette activité d’échange pédagogique vise à faire connaitre les ressources éducatives libres (REL), comment on peut intégrer des REL existantes à notre enseignement, comment on peut en adapter pour répondre à nos besoins pédagogiques et quelle démarche peut être entreprise pour créer notre propre REL au besoin.
 
Les ressources éducatives libres (REL) sont «des matériels d’enseignement, d’apprentissage ou de recherche appartenant au domaine public ou publiés avec une licence de propriété intellectuelle permettant leur utilisation, adaptation et distribution à titre gratuit».