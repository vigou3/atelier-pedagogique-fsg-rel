### -*-Makefile-*- pour préparer "Ressources éducatives libres (REL)"
##
## Copyright (C) 2023 Vincent Goulet
##
## Ce fichier fait partie du projet «Atelier d'échange pédagogique de
## la Faculté des sciences et de génie - Ressources éducatives
## libres» <https://gitlab.com/vigou3/atelier-pedagogique-fsg-rel>
##
## Cette création est mise à disposition selon le contrat
## Attribution-Partage dans les mêmes conditions 4.0 International de
## Creative Commons. <https://creativecommons.org/licenses/by-sa/4.0>
##
## 'make pdf' compile le document maitre avec XeLaTeX.
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .pdf.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make pdf' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet

## Principaux fichiers
MASTER = atelier-pedagogique-fsg-rel.pdf
README = README.md
NEWS = NEWS
LICENSE = LICENSE

## Le document maitre dépend de tous les fichiers .tex autres que
## lui-même.
TEXFILES = $(addsuffix .tex,$(filter-out $(basename ${MASTER}),\
                                         $(basename $(wildcard *.tex))))

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep "\\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
RM = rm -rf

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: pdf

FORCE:

${MASTER}: ${MASTER:.pdf=.tex} ${TEXFILES} $(wildcard images/*)
	${TEXI2DVI} ${MASTER:.pdf=.tex}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: release
release: pdf check-status create-release upload create-link

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "vérification de l'état du dépôt local... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "! pas sur la branche main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "! changements non archivés dans le dépôt"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "changements non publiés dans le dépôt; publication dans origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "vérification que la version existe déjà... "; \
	    http_code=$$(curl -I "${APIURL}/releases/${TAGNAME}" 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "oui"; \
	        printf "%s\n" "-> utilisation de la version actuelle"; \
	    else \
	        printf "%s\n" "non"; \
	        printf "%s" "création d'une version dans GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print "Édition", $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "téléversement de l'archive vers le registre..."
	curl --upload-file "${MASTER}" \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/${REPOSNAME}/${VERSION}/${ARCHIVE}"
	@printf "\n%s\n" "ok"

.PHONY: create-link
create-link:
	@printf "%s\n" "ajout du lien dans la description de la version..."
	$(eval PKG_ID=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval FILE_ID=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${PKG_ID}/package_files" \
	                       | grep -o -E '\{[^{]*"file_name":"${MASTER}"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval URL="${REPOSURL:/=}/-/package_files/${FILE_ID}/download")
	@printf "  lien vers %s: %s\n" "${MASTER}" "${URL}"
	@curl --request POST \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --data name="${MASTER}" \
	      --data url="${URL}" \
	      --data link_type="other" \
	      --output /dev/null --silent \
	      "${APIURL}/releases/${TAGNAME}/assets/links"
	@printf "%s\n" "ok"

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES}
	@printf "%s\n" "vérification des adresses URL dans les fichiers source"
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '>' -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq))
	@for u in ${url}; do \
	    printf "%s... " "$$u"; \
	    if curl --output /dev/null --silent --head --fail --max-time 5 "$$u"; then \
	        printf "%s\n" "ok"; \
	    else \
		printf "%s\n" "invalide ou ne répond pas"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      *.aux *.log *.snm *.nav *.vrb *.toc *.out
